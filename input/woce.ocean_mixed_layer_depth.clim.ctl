dset ^woce.ocean_mixed_layer_depth.clim.bin
undef 9.e27
xdef  96 linear 0 3.75
ydef  48 linear -88.125 3.75
zdef   1 linear 1 1
tdef 730 linear 00:00Z01jan2000  12hrs
vars 1
mld  1 0 data 1
endvars
