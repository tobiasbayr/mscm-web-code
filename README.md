# README #
### What is this repository for? ###

This repository contains the model code of the Globally Resolved Energy Balance (GREB) model,
which is the climate model used for the experiments shown on the Monash Simple Climate Model (MSCM) homepage
( http://mscm.dkrz.de or http://monash.edu/research/simple-climate-model/mscm/ )

### How do I get set up? ###

You can download this model code  including all required input files in your own git repository. All you need is git to be installed on your computer.
Once you installed git, create a folder for the code and initiate the repository with:

git init

To download the repository:

git pull https://bitbucket.org/tobiasbayr/mscm-web-code.git master

OR: You can download the model code including all required input files as compressed tar archive from the MSCM homepage:

http://mscm.dkrz.de/download/mscm-web-code.tar.gz

To get the model running, see read.me.txt in main folder.

### Distribution guidelines ###

This is a public repository, i.e. that the model code is free to be used by anyone.

### Citation ###

Please cite the following references:

Conceptual Understanding of Climate Change with a 
Globally Resolved Energy Balance Model
by Dietmar Dommenget and Janine Flöter, J. Clim Dyn (2011) 37: 2143. 
doi:10.1007/s00382-011-1026-0

The Monash Simple Climate Model Experiments: An interactive database 
of the mean climate, climate change and scenarios simulations
by Dietmar Dommenget, Kerry Nice, Tobias Bayr, Dieter Kasang, Christian Stassen 
and Mike Rezny, submitted in 03/2018 to Geoscientific Model Development

### Who can I talk to? ###

Tobias Bayr (tbayr@geomar.de) or Dietmar Dommenget (dietmar.dommenget@monash.edu)